(asdf:defsystem #:swm-weather
  :serial t
  :author "soup"
  :version "0.1.0"
  :license "GPL v3.0"
  :depends-on (:stumpwm :cl-ppcre :dexador :s-xml)
  :components ((:file "src/package")
               (:file "src/swm-weather")))
