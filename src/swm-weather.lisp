(in-package #:swm-weather)

(defparameter weather-source "http://w1.weather.gov/xml/current_obs/~d.xml"
  "NOAA XML values for weather, with a FORMAT directive for taking in
  a value and substituting it in to narrow down to the region within
  the United States.")

(defparameter weather-keywords (list :weather :temperature-string
                                     :temp-f :temp-c
                                     :relative-humidity :wind-string
                                     :wind-dir :wind-degrees
                                     :wind-mph :wind-kt
                                     :pressure-string :pressure-mb
                                     :pressure-in :dewpoint-string
                                     :dewpoint-f :dewpoint-c
                                     :windchill-string :windchill-f
                                     :windchill-c :visibility-mi)
  "Keywords for values in the XML stream from NOAA")

(defvar *latest-weather* nil)

(defvar *noaa-code* "KICT"
  "Default is as close to Center, Kansas as possible.
Find your NOAA city code and SETF it in your stumpwm conf.")

(defvar *weather-fields* (list :weather :temp-f)
  "Define which values are displayed in the mode-line by default,
  customize by SETF-ing from the WEATHER-KEYWORDS list below.")

(defun lispify-keyword (keyword)
  "Take a symbol `KEYWORD', replace underscores with dashes"
  (let ((cl:*read-eval* nil))
    (read-from-string (concatenate 'string ":"
                                   (cl-ppcre:regex-replace-all "_" (symbol-name keyword) "-")))))

(defun lispify-keywords (alist)
  (loop :for row :in alist
        :collecting (list (lispify-keyword (car row))
                          (cadr row))))

(defun get-weather ()
  (lispify-keywords
   (s-xml:parse-xml-string
    (dex:get (format nil weather-source *noaa-code*)
             :insecure t
             :use-connection-pool t
             :keep-alive nil))))

(defun make-weather-alist (noaa-sxml-data)
  (loop :for noaa-line :in noaa-sxml-data
        :when (member (car noaa-line) weather-keywords)
          :collect noaa-line))

(defun update-weather ()
  (let ((next-update (get-weather)))
    (when next-update 
      (setf *latest-weather* (make-weather-alist next-update)))))

;;; 10 second startup delay, run every half-hour
(defparameter swm-weather-timer
  (stumpwm:run-with-timer 10 (* 30 60)
                          (lambda ()
                            (bordeaux-threads:make-thread #'update-weather
                                                          :name (symbol-name (gensym "update-weather-"))))))

(defun fmt-weather (ml)
  (declare (ignore ml))
  (format nil "~{~D~^, ~}"
          (loop :for elt :in *latest-weather*
                :when (member (car elt) *weather-fields*)
                  :collect (cadr elt))))

(stumpwm:add-screen-mode-line-formatter #\M #'fmt-weather)

;;; EOF
