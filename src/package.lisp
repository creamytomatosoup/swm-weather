(defpackage #:swm-weather
  (:use :cl)
  (:export :*weather-fields*
           :*noaa-code*))
